this version uses index1.html, script.js and style.css to 
show a list of tasks.
the html has a place holder for a rendered html that will 
come from the script.
the script has methods for showing, adding, updating and deleting.
These methods uses REST apis from the server
At the end of each method it calls the method that renders the list.
the script uses local storage for the counter for the id but not for
the list itself. in fact it does not store the list as it is stored in
the server.
The next version will use a server.