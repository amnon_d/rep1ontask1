// A task object: {id: number, name: string, isDone: Boolean}
const list = localStorage.listOfTasks ? JSON.parse(localStorage.listOfTasks) : [];
let id_counter = Number(localStorage.taskIdCounter ? JSON.parse(localStorage.taskIdCounter) : 0);
let to_render = '';
renderList();


function renderList() {
    to_render = "";

    for (let i = 0; i < list.length; i++) {
        to_render += `<div class="box"><label>${list[i].name}</label> <div>
        <input type="checkbox" id="${list[i].id}" onchange="updateTask(id)"`;
        to_render += list[i].isDone ? " checked >" : ">";
        to_render += `<button id="${list[i].id}" onclick="deleteTask(id)" >X</button> </div> </div> `
    }

    console.log(to_render);
    document.querySelector('#TaskList').innerHTML = to_render;
    localStorage.listOfTasks = JSON.stringify(list);
    localStorage.taskIdCounter = JSON.stringify(id_counter);
}


function addNewTask(event) {
    event.preventDefault();
    const values = Object.values(event.target);

    list.push({ id: id_counter++, name: values[0].value, isDone: false });

    console.log(...list);
    renderList();
}

function deleteTask(id) {
    for (let i = 0; i < list.length; i++) {
        if (list[i].id == id) {
            list.splice(i, 1);
            break;
        }
    }

    console.log(...list)
    renderList();
}

function updateTask(id) {
    console.log(id);
    for (let i = 0; i < list.length; i++) {
        if (list[i].id == id) {
            list[i].isDone = !list[i].isDone;
            break;
        }
    }
    console.log(...list)
    renderList();
}


document.querySelector('form').onsubmit = addNewTask;


